python-ete3 (3.1.3+dfsg-3) unstable; urgency=medium

  * Team upload.
  * python3.13.patch: new: fix test failure with Python 3.13.
    (Closes: #1082310)
  * fix-warnings.patch: new: fix a sprawl of SyntaxWarnings.
  * d/s/lintian-overrides: fix false positive source missing.
  * d/control: declare compliance to standards version 4.7.0.

 -- Étienne Mollier <emollier@debian.org>  Sat, 21 Sep 2024 15:13:21 +0200

python-ete3 (3.1.3+dfsg-2) unstable; urgency=medium

  * Team upload.
  * python3.12.patch: new: fix test failures. (Closes: #1058334)

 -- Étienne Mollier <emollier@debian.org>  Thu, 14 Dec 2023 18:33:02 +0100

python-ete3 (3.1.3+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 15 Nov 2023 11:26:38 +0100

python-ete3 (3.1.2+dfsg-3) unstable; urgency=medium

  * Team upload.

  [ Jochen Sprickerhof ]
  * Drop unused (Build-)Dependency python3-qt-binding

  [ Andreas Tille ]
  * Exclude i386 from CI since Build-Depends python3-skbio is not available
    on i386
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 28 Oct 2021 07:24:27 +0200

python-ete3 (3.1.2+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Moved python3-numpy from Recommends to Depends.  (Closes: #972983)
  * Restored ete3/tools:
    - it allowed autodep8 autopkgtest to succeed,
    - pmodeltest_executable.patch wrapped up to address the reintroduced
      executable not ELF nor script.
  * Added repository information to upstream/metadata.
  * Set upstream metadata fields: Bug-Submit.

 -- Étienne Mollier <etienne.mollier@mailoo.org>  Wed, 04 Nov 2020 19:21:18 +0100

python-ete3 (3.1.2+dfsg-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * New upstream version
  * Update copyright

  [ zhao feng ]
  * Exclude test test_merged_id

 -- Andreas Tille <tille@debian.org>  Wed, 30 Sep 2020 08:46:10 +0200

python-ete3 (3.1.1+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Add missing Depends

 -- Andreas Tille <tille@debian.org>  Thu, 16 Jul 2020 14:08:06 +0200

python-ete3 (3.1.1+dfsg-1) unstable; urgency=medium

  * Initial release. (Closes: #861182, #963732)

 -- zhao feng <zhaofengshu33@gmail.com>  Sun, 05 Jul 2020 12:18:02 +0200
